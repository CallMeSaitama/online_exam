<?php
 //entry.php
 include("../inc/functions.php");
 session_start();

 if(!isset($_SESSION["customer_email"]))
 {
      header("location:../index.php");
      
  
    }
 else {
     $space = " ";
     $per = "%";
     $customer_email = $_SESSION["customer_email"]; 
     $con = vmf_connect_db();
     $sql = "select id, firstname, lastname, phone, user_elo from info WHERE email='" . $customer_email . "'";
        
        $query = mysqli_query($con, $sql);
        while($rs = mysqli_fetch_assoc($query)){
            $user_id = $rs['id'];
            $user_firstname = $rs['firstname'];
            $user_lastname = $rs['lastname'];
            $user_phone = $rs['phone'];
            $user_elo = $rs['user_elo'];
         }
 
 }
    
    $percentage= 0;
    $t1 = $_GET['test'];
    $name = $_GET['name'];
    $q1 = "t/t";
    $q2 = ".txt";
    $ques= $q1.$t1.$q2;
    
    
    $urlquery = 'test=' . $t1 . '&' . 'name='. $name;
  
    
 ?>

<?php

	
// Read answerkey.txt file for the answers to each of the questions.
function readAnswerKey($filename) {
	$answerKey = array();
	
	// If the answer key exists and is readable, read it into an array.
	if (file_exists($filename) && is_readable($filename)) {
		$answerKey = file($filename);
	}
	
	return $answerKey;
}


// Read the questions file and return an array of arrays (questions and choices)
// Each element of $displayQuestions is an array where first element is the question 
// and second element is the choices.

function readQuestions($filename) {
	$displayQuestions = array();
	
	if (file_exists($filename) && is_readable($filename)) {
		$questions = file($filename);
	
		// Loop through each line and explode it into question and choices
		foreach ($questions as $key => $value) {
			$displayQuestions[] = explode(":",$value);
		}				
	}
	else { echo "Error finding or reading questions file."; }
	
	return $displayQuestions;
}


// Take our array of exploded questions and choices, show the question and loop through the choices.
function displayTheQuestions($questions) {
	if (count($questions) > 0) {
		foreach ($questions as $key => $value) {
			echo "<b>$value[0]</b><br/><br/>";
			
			// Break the choices appart into a choice array
			$choices = explode(",",$value[1]);
			
			// For each choice, create a radio button as part of that questions radio button group
			// Each radio will be the same group name (in this case the question number) and have
			// a value that is the first letter of the choice.
			
			foreach($choices as $value) {
				$letter = substr(trim($value),0,1);
				echo "<input type=\"radio\" name=\"$key\" value=\"$letter\" style=\"text-align:left;\">$value<br/>";
			}
			
			echo "<br/>";
		}
	}
	else { echo "No questions to display."; }
}


// Translates a precentage grade into a letter grade based on our customized scale.
function translateToGrade($percentage) {

	if ($percentage >= 90.0) { return "A"; }
	else if ($percentage >= 80.0) { return "B"; }
	else if ($percentage >= 70.0) { return "C"; }
	else if ($percentage >= 60.0) { return "D"; }
	else { return "F"; }
}

?>

<!DOCTYPE html>
<html lang="en">

  <head>
   

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Online Examinations System</title>

    <!-- Bootstrap core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <style>
      body {
        padding-top: 54px;
      }
      @media (min-width: 992px) {
        body {
          padding-top: 56px;
        }
      }

    </style>

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" style="background-color: #5cb85c !important;">
      <div class="container">
        <a class="navbar-brand" href="#">Online Examinations System</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="../dashboard/index.php">Home
                
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link active" href="../test.php">Tests
                <span class="sr-only">(current)</span>
                </a>
                
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../profile.php">Profile</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../logout.php">Logout</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h1 class="mt-5" id="testname">Test For <?= $name ?> Here</h1>
          <p class="lead"><?= $a3 ?></p>
          
          
          <form method="POST" action="<?php echo $_SERVER["PHP_SELF" . '?' . $urlquery]; ?>" id="todelete">
              
              <?php
	// Load the questions from the questions file
    
//	$loadedQuestions = readQuestions("t1.txt");
             
//           
              $loadedQuestions = readQuestions($ques);
               
	
	// Display the questions
	displayTheQuestions($loadedQuestions);
?>

        <input  class="btn btn-success" type="submit" name="submitquiz" value="Submit Quiz"/>
            </form>
          
            

<?php

#Writing The score intoto database 

   
// This grades the quiz once they have clicked the submit button
if (isset($_POST['submitquiz'])) {
   $con = vmf_connect_db();
    
    
    //delete questions
         echo '
        <br>
        <a href="../test.php" class="btn btn-primary" role="button">Test Dashboard</a>
        <script>
        console.log("i should be working and deleting");
        var element = document.getElementById("todelete");
        var element1 = document.getElementById("testname");
        element.parentNode.removeChild(element);
        element.parentNode.removeChild(element1);
        </script>'; 
  
 

	// Read in the answers from the answer key and get the count of all answers.
  
    $q1 = "a/a";
    $q2 = ".txt";
    $user_current = $_GET['test'];
    $ans= $q1.$user_current.$q2;
    
	$answerKey = readAnswerKey($ans);
	$answerCount = count($answerKey);
	$correctCount = 0;
	


	// For each answer in the answer key, see if the user has a matching question submitted
	foreach ($answerKey as $key => $keyanswer) {
		if (isset($_POST[$key])) {
			// If the answerkey and the user submitted answer are the same, increment the 
			// correct answer counter for the user
			if (strtoupper(rtrim($keyanswer)) == strtoupper($_POST[$key])) {
				$correctCount++;
			}
		}
	}

    
	// Now we know the total number of questions and the number they got right. So lets spit out the totals.
	echo "<br/><br/>Total Questions: $answerCount<br/>";
	echo "Number Correct: $correctCount<br/><br/>";
	
	if ($answerCount > 0) {
    
		// If we had answers in the answer key, translate their score to percentage
		// then pass that percentage to our translateGrade function to return a letter grade.
		$percentage = round((($correctCount / $answerCount) * 100),1);
		echo "Total Score: $percentage% (Grade: " . translateToGrade($percentage) . ")<br/>";
        
        
        // elo ranking changes here
        $test_id = "t$user_current";
    
        
        $sqltest_elo = "SELECT * FROM test_rank WHERE test_id= $user_current";
      
        $query3 = mysqli_query($con, $sqltest_elo) or die("Q dfail");
       while($rst = mysqli_fetch_assoc($query3) ){
            $test_ = $rst['test_id'];
            $test_elo = $rst['test_elo'];
         }
        
         //elo ranking changes here
        //probabilities of winning    
         $p1 = $user_elo;
         $rating1 = $p1*1000;
         
         $p2 =   $test_elo;
         $rating2 = $p1*1000;

         // elo constant 30 due to change with testing
         $k = 7;
        
        if ($percentage >= 40){
        //user passes    
            $rating1 = $rating1 + $k*(1 - $p1);
            $rating2 = $rating2 + $k*(0 - $p2);
            
        }
        else {
        //user loses    
            $rating1 = $rating1 + $k*(0 - $p1);
            $rating2 = $rating2 + $k*(1 - $p2);
            
        }
        $rating1 /= 1000;
        $rating2 /= 1000;
       
       
         $sqltestupdate = "UPDATE test_rank SET test_elo = $rating2 WHERE test_id= $user_current";
        
   		$querytestupdate = mysqli_query($con,$sqltestupdate) or die("test update failed"); 
        
        $sqluserupdate = "UPDATE info SET user_elo = $rating1 WHERE id= $user_id";
        $queryuserupdate = mysqli_query($con,$sqluserupdate)  or die("test update failed2");
        
       
          
	}
	else {
		// If something went wrong or they failed to answer any questions, we have a score of 0 and an "F"
		echo "Total Score: 0 (Grade: F)";
	}
    $sqltest = "UPDATE test SET t$user_current = $percentage WHERE id= $user_id";
        
   		$querytest = mysqli_query($con,$sqltest); 
}

?>         
            
          
          </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>
