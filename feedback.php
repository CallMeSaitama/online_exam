<?php
 //entry.php
 include("inc/functions.php");
 session_start();

 if(!isset($_SESSION["customer_email"]))
 {
      header("location:index.php");
      
  
    }
 else {
    $customer_email = $_SESSION["customer_email"]; 
    $space = " "; 
   $con = vmf_connect_db();
   $sql = "select id, firstname, lastname, phone from info WHERE email='" . $customer_email . "'";

    $query = mysqli_query($con, $sql);
    while($rs = mysqli_fetch_assoc($query)){
        $user_id = $rs['id'];
        $user_firstname = $rs['firstname'];
        $user_lastname = $rs['lastname'];
        $user_phone = $rs['phone'];
        
     }
 }
 ?>

<!DOCTYPE html>
<html>
    <head>
          <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-116564296-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-116564296-1');
        </script>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Online Examination System</title>

         <!-- Bootstrap CSS CDN -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" integrity="sha384-3AB7yXWz4OeoZcPbieVW64vVXEwADiYyAEhwilzWsLw+9FgqpyjjStpPnpBO8o8S" crossorigin="anonymous">
        <!-- Our Custom CSS -->
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="css/dashboard.css">
    </head>
    <body>



        <div class="wrapper">
            <!-- Sidebar Holder -->
            <nav id="sidebar">
                <div class="sidebar-header text-center">

                    <h3>Online System</h3>
                    
                </div>

                <ul class="list-unstyled components">
                    <p>Learning Platform</p>
                    <li class="active">
                        <a href="dashboard/index.php" data-toggle="collapse" aria-expanded="false">Home</a>
                        <ul class="collapse list-unstyled" id="homeSubmenu">
                            <li><a href="test.php">Tests</a></li>
                            <li><a href="misctest.php">Tests</a></li>
                            <li><a href="profile.php">Profile</a></li>
                            <li><a href="logout.php">Logout</a></li>
                        </ul>
                    </li>
                    <li>
                    
                        <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false">Learning Modules</a>
                        <ul class="collapse list-unstyled" id="pageSubmenu">
                            <li><a href="page-chat.php">Learning Modules</a></li>
                            <li><a href="https://snatchbot.me/webchat/?botID=12490&appID=Qd5lQiEQxs4GPleMMndJ">Practice Engine</a></li>
                            
                        </ul>
                        <a href="feedback.php">Feedback</a>
                        <a href="#">About</a>
                    </li>
                    
                </ul>

                <ul class="list-unstyled CTAs">

                </ul>
            </nav>

            <!-- Page Content Holder -->
            <div id="content">

                <nav class="navbar navbar-default text-center">
                    <div class="container-fluid">

                        <div class="navbar-header">
                            <button type="button" id="sidebarCollapse" class="navbar-btn">
                                <span></span>
                                <span></span>
                                <span></span>
                            </button>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="dashboard/index.php" >Dashboard</a></li>
                                <li><a href="test.php">Tests</a></li>
                                <li><a href="profile.php" style="color: #23527c;">Profile</a></li>
                                <li><a href="logout.php">Logout</a></li>
                                
                            </ul>
                        </div>
                    </div>
                </nav>

                <h2>Feedback</h2>
              
                <div class="line"></div>
                <div class="row text-left" style="color:black !important;padding-left:5%;padding-right:5%;">
                            <h2>Your valuable to be filled below!</h2><br>

                    <div style="display:none" id="thankyou_message">
                    <h2>Thanks for contacting us. I will get back to you soon.</h2>
                    </div>
                    <form method="GET" action="<?php echo $_SERVER["PHP_SELF"]?>" id="todelete">

                    <div class="messages"></div>

                    <div class="controls">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_name">Firstname *</label>
                                    <input id="form_name" type="text" name="fname" class="form-control" 
                                    placeholder="Enter your name here.." required="required" data-error="Firstname is required.">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_lastname">Lastname *</label>
                                    <input id="form_lastname" type="text" name="lname" class="form-control" 
                                    placeholder="Enter your name here.."" required="required" data-error="Lastname is required.">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_email">Email *</label>
                                    <input id="form_email" type="email" name="email" class="form-control" 
                                    placeholder="<?php echo "$customer_email"; ?>" required="required" data-error="Valid email is required." disabled>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_phone">Phone</label>
                                    <input id="form_phone" type="tel" name="phone" class="form-control" 
                                    placeholder="<?php echo "$user_phone"; ?>" disabled>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="form_message">Message *</label>
                                    <textarea id="form_message" name="message" class="form-control" placeholder="Message for me *" rows="4" required="required" data-error="Please,leave us a message."></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <input type="submit" class="btn btn-success btn-send" name="submit" value="Send message">
                            </div>
                        </div>
                        
                    </div>

                    </form>
                          </div>


                    <!-- Submit the Form to Google Using "AJAX" -->
                    <script data-cfasync="false" type="text/javascript"
                    src="https://cdn.rawgit.com/dwyl/learn-to-send-email-via-google-script-html-no-server/master/form-submission-handler.js"></script>
                    <!-- <script data-cfasync="false" type="text/javascript"
                    src="/form-submission-handler.js"></script> -->
                  <!-- END -->
             
                  
                  <?php

#Writing The score intoto database 

   
            // This grades the quiz once they have clicked the submit button
            if (isset($_GET['submit'])) {
                  mail_feedback();
                }
                 ?>
                

                <div class="line"></div>


            </div>
        </div>



        <!-- jQuery CDN -->
         <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
         <!-- Bootstrap Js CDN -->
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
         <script type="text/javascript">
             $(document).ready(function () {
                 $('#sidebarCollapse').on('click', function () {
                     $('#sidebar').toggleClass('active');
                     $(this).toggleClass('active');
                 });
             });
         </script>
        <script type="text/javascript">
         var height = window.innerHeight ;  
         var sub = 0.2*height;    
            document.getElementById("iframe1").height = height-sub;
        
        
        
        </script>
    </body>
</html>
