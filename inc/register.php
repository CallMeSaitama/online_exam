<?php  

require_once('functions.php');
if ( isset( $_POST['formSubmit'] ) ) {
    
 


    $user_does_not_exist = false;
    $error = false;

    $customer_name1 = isset( $_POST['customer_name1'] ) ? $_POST['customer_name1'] : null;
    $customer_name2 = isset( $_POST['customer_name2'] ) ? $_POST['customer_name2'] : null;
    $customer_phone = isset( $_POST['customer_phone'] ) ? $_POST['customer_phone'] : null;
    $customer_email = isset( $_POST['customer_email'] ) ? $_POST['customer_email'] : null;
    $customer_pass = isset( $_POST['customer_pass'] ) ? $_POST['customer_pass'] : null;
    $customer_repass = isset( $_POST['customer_repass'] ) ? $_POST['customer_repass'] : null;
    $is_registered = false;
    $message = false;

    if( $customer_name1 AND $customer_name2 AND $customer_phone AND $customer_email AND $customer_pass AND $customer_repass ) {

        if ( check_user_exists( $customer_email ) ) {

            $error = "User already exists.";

        } else {

            if( $customer_repass == $customer_pass ) {
                // register
                if( $is_registered = register_chat_user( $customer_name1, $customer_name2, $customer_email, $customer_pass , $customer_phone ) ) {
                  header('Location: ../index.php?message=Successfully%20Registered');    
                  echo "Member successfully registered. <a href='../index.php'>Click here to login.</a>";
                } else {
                    $error = "Registration failed. Please check all details";
                }
            } else {
                $error = "Please re-type the correct password";
            }
        }
    } else {
        $error = "Please enter all details";
    }
}
