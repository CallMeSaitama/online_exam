<?php

  require_once('inc/functions.php');

  if(isset($_SESSION["customer_email"]))
  {
       header("location:page-chat.php");
  }
  


?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="noindex, nofollow">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">

    <title>Online Exam Registeration Page</title>
  </head>

  <body class="text-center">
    <div class="container">

        <div class="form-signin">
   
        </div>
        <form class="form-register col-sm-6 offset-sm-3" action="inc/register.php" method="post">
            <h1> Registration Page</h1>
            <img class="mx-auto d-block mb-3" src="" alt="">

            <div class="form-group">
                <label for="inputName" class="sr-only">Your Name</label>
                <input type="text" id="inputName1" class="form-control form-control-lg" name="customer_name1" placeholder="First Name" required autofocus>
            </div>

            <div class="form-group">
                <label for="inputName" class="sr-only">Your Name</label>
                <input type="text" id="inputName2" class="form-control form-control-lg" name="customer_name2" placeholder="Last Name" required autofocus>
            </div>

            <div class="form-group">
                <label for="inputPhone" class="sr-only">Your Phone Number</label>
                <input type="tel" id="inputEmail" class="form-control form-control-lg" name="customer_phone" placeholder="Your Phone Number" required>
            </div>

            <div class="form-group">
                <label for="inputEmail" class="sr-only">Email address</label>
                <input type="email" id="inputEmail" class="form-control form-control-lg" name="customer_email" placeholder="Email address" required>
            </div>

            <div class="form-group">
                <label for="inputPassword" class="sr-only">Password</label>
                <input type="password" id="inputPassword" name="customer_pass" class="form-control form-control-lg" placeholder="Password" required>
            </div>

            <div class="form-group">
                <label for="inputPassword" class="sr-only">Re-type Password</label>
                <input type="password" id="inputPassword" name="customer_repass" class="form-control form-control-lg" placeholder="Re-Type Password" required>
            </div>
               <div class="form-group">
           <div class="g-recaptcha text-center" data-sitekey="6Ld3AlIUAAAAABlq2OFN0oMq0_dYsUQ_oanhP7Bm" style="width:100%"></div>
            </div>


            <div class="form-group">
                <button class="btn btn-lg btn-warning btn-block" name="formSubmit" type="submit">Register</button>
                <br>
                <a href="index.php" class="text-white">Already have an account? Login here</a>
                <p class="mt-2 mb-2 text-white font-weight-light">&copy; <?php echo date('Y'); ?></p>
            </div>
        </form>


    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
      <script src='https://www.google.com/recaptcha/api.js'></script>
  </body>
</html>
