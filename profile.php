<?php
 //entry.php
 include("inc/functions.php");
 session_start();

 if(!isset($_SESSION["customer_email"]))
 {
      header("location:index.php");
      
  
    }
 else {
    $customer_email = $_SESSION["customer_email"]; 
    $space = " "; 
   $con = vmf_connect_db();
   $sql = "select id, firstname, lastname, phone from info WHERE email='" . $customer_email . "'";

    $query = mysqli_query($con, $sql);
    while($rs = mysqli_fetch_assoc($query)){
        $user_id = $rs['id'];
        $user_firstname = $rs['firstname'];
        $user_lastname = $rs['lastname'];
        $user_phone = $rs['phone'];
        
     }
 }
 ?>

<!DOCTYPE html>
<html>
    <head>
          <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-116564296-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-116564296-1');
        </script>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Online Examination System</title>

         <!-- Bootstrap CSS CDN -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" integrity="sha384-3AB7yXWz4OeoZcPbieVW64vVXEwADiYyAEhwilzWsLw+9FgqpyjjStpPnpBO8o8S" crossorigin="anonymous">
        <!-- Our Custom CSS -->
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="css/dashboard.css">
    </head>
    <body>



        <div class="wrapper">
            <!-- Sidebar Holder -->
            <nav id="sidebar">
                <div class="sidebar-header text-center">
                    <h3>Profile Section</h3>

                </div>
    <ul class="list-unstyled components">
                    <p>Learning Platform</p>
                    <li class="active">
                        <a href="dashboard/index.php" data-toggle="collapse" aria-expanded="false">Home</a>
                        <ul class="collapse list-unstyled" id="homeSubmenu">
                            <li><a href="test.php">Tests</a></li>
                            <li><a href="profile.php">Profile</a></li>
                            <li><a href="logout.php">Logout</a></li>
                        </ul>
                    </li>
                    <li>

                        <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false">Learning Modules</a>
                        <ul class="collapse list-unstyled" id="pageSubmenu">
                            <li><a href="page-chat.php">Learning Modules</a></li>
                            <li><a  href="page-chat.php" data-toggle="modal" data-target="#practice">Practice Engine</a></li>

                        </ul>
                        <a href="#">About</a>
                    </li>

                </ul>
                <ul class="list-unstyled CTAs">

                </ul>
            </nav>

            <!-- Page Content Holder -->
            <div id="content">

                <nav class="navbar navbar-default text-center">
                    <div class="container-fluid">

                        <div class="navbar-header">
                            <button type="button" id="sidebarCollapse" class="navbar-btn">
                                <span></span>
                                <span></span>
                                <span></span>
                            </button>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                             <li><a href="dashboard/index.php" style="color: #23527c;">Dashboard</a></li>
                                <li><a href="test.php">Tests</a></li>
                                <li><a href="misctest.php">Practice Engine</a></li>
                                <li><a href="profile.php" style="color: #23527c;">Profile</a></li>
                                <li><a href="logout.php">Logout</a></li>

                            </ul>
                        </div>
                    </div>
                </nav>

                <h2><?= $user_firstname ;?>'s Profile</h2>

                <div class="line"></div>




                <div class="row text-left" style="color:black !important;padding-left:10%;font-size:1.5em;">
                   <h2>Profile Information</h2>
                    <p  style="color:black !important;"><strong>Name : </strong> <?= $user_firstname . $space . $user_lastname ;?> </p>
                    <p style="color:black !important;"><strong>Email : </strong> <?= $_SESSION["customer_email"];?> </p>
                    <p style="color:black !important;"><strong>Phone : </strong> <?= $user_phone;?> </p>


                </div>




                <div class="line"></div>

                <h3></h3>
                <p style="color:white">randhuygviudbvdivbniudbvniudfjhbvniujkfbvnhijkbvnifujbvniufdbvniufjkbvnhiufdjghbvnuiop;ghbnopiuahgnauiopshgnuoaghhjjhghgfc</p>
            </div>
        </div>
        
        
        
        



        <!-- jQuery CDN -->
         <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
         <!-- Bootstrap Js CDN -->
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    </body>
</html>
