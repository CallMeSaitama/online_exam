<?php
 //entry.php
 include("../inc/functions.php");
 session_start();

 if(!isset($_SESSION["customer_email"]))
 {
      header("location:../index.php");
    }
 else {
     $customer_email = $_SESSION["customer_email"]; 
     $con = vmf_connect_db();
     $sql = "select id, firstname, lastname, phone, user_elo from info WHERE email='" . $customer_email . "'";   
        $query = mysqli_query($con, $sql);
        while($rs = mysqli_fetch_assoc($query)){
            $user_id = $rs['id'];
            $user_firstname = $rs['firstname'];
            $user_lastname = $rs['lastname'];
            $user_phone = $rs['phone'];
            $user_elo = $rs['user_elo'];
         }
     
     $sqltest = "select * from test WHERE id='" . $user_id . "'";
     $querytest = mysqli_query($con, $sqltest);
     
    
     
        if(!$_SESSION['wrong'] && !$_SESSION['rigt']&& !$_SESSION['attempt']){
            $_SESSION['wrong']=0;
            $_SESSION['correct']=0;
            $_SESSION['attempt']=0;
            $_SESSION['average']=0;
            unset($value2);
        }
    
   if(isset($_POST['digits'])){
            $_SESSION['digits']=$_POST['digits'];
   }
        if(isset($_POST['questions'])){
           $_SESSION['questions']=$_POST['questions'];
            $_SESSION['totalquestions']=$_SESSION['questions'];
            
        }
    
     

        $sign = $_GET['sign'];
     // randomize number according to digits
        if($_SESSION['digits']==2){
        $value1=mt_rand(20,99);
        $_SESSION['value1']=$value1;        
        }
        elseif($_SESSION['digits']==3){
        $value1=mt_rand(150,999); 
        $_SESSION['value1']=$value1;
        }
     
        elseif($_SESSION['digits']==4 ){
            $value1=mt_rand(1100,9999);
            $_SESSION['value1']=$value1;
            
            if( $_SESSION['digits']==4 && (strpos($sign,"cubes") !== false) || (strpos($sign,"cr") !== false) ) {
        $value1=mt_rand(150,999);
        $_SESSION['value1']=$value1;

        }
        


        }
 }
    $name = $_GET['name'];    
    $urlquery = 'sign=' . $sign . '&' . 'name='. $name. '&' .'attempt='.$_SESSION['attempt'];
    
    $count=0;
    $start=0;
    $_SESSION['sign']=$sign;
    $_SESSION['url']=$urlquery;
    $start=microtime(true);
    $_SESSION['start']=$start;    
    if (strpos($sign,"sqr") !== false) {
        $_SESSION['result']= round(sqrt($value1),2);
    }
    elseif (strpos($sign,"cr") !== false) {
        $_SESSION['result']= round(pow($value1, 1/3),2);
    }
    elseif(strpos($sign,"squares") !== false){
        $_SESSION['result']= pow($value1,2);
    }
    elseif(strpos($sign,"cubes") !== false){
        $_SESSION['result']= pow($value1,3);
    }

    if (strpos($sign,"sqr") !== false) {
         $_SESSION['dbset']= 'p8';
        
            }
        elseif (strpos($sign,"cr") !== false) {
             $_SESSION['dbset']= 'p9';
            
            }
        elseif(strpos($sign,"squares") !== false){
             $_SESSION['dbset']= 'p6';
       
            }
        elseif(strpos($sign,"cubes") !== false){
            $_SESSION['dbset']= 'p7';
            }
 ?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Online Examinations System</title>
    <!-- Bootstrap core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
      <link href='https://fonts.googleapis.com/css?family=Orbitron' rel='stylesheet' type='text/css'>
    <!-- Custom styles for this template -->
      <style>
      body {
        padding-top: 54px;
      }
      @media (min-width: 992px) {
        body {
          padding-top: 56px;
        }
      }
    </style>
      
  </head>
  <body >
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" style="background-color: #1e72bd !important;">
      <div class="container">
        <a class="navbar-brand" href="#">Online Test</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="../dashboard/index.php">Home   
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link active" href="../test.php">Tests
                <span class="sr-only">(current)</span>
                </a>      
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../profile.php">Profile</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../logout.php">Logout</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- Page Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h1 class="mt-5" id="testname">Test For <u><?= $name ?></u> Here</h1>
          <p class="lead"><?= $a3 ?></p> 
          <form method="POST" action="<?php echo "fscsca.php".'?'.$_SESSION['url']; ?>" id="todelete">
              <style>
            hr {
                width: 20%;
                height: 1px;
                margin-left: auto;
                margin-right: auto;
                background-color:black;
                border: 0 none;
                }
                  
                @media (max-width: 400px) {
               h1:first-of-type {
          padding-left: 5%;
        }
      }
              </style>
              
              <?php
//date_default_timezone_set('UTC+5:30hr');
?>
<script>
function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    setInterval(function () {
        minutes = parseInt(timer / 60, 10)
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;
        document.getElementById("time").style.fontSize = "x-large";
        display.textContent = minutes + ":" + seconds;

        if (++timer < 0) {
            timer = duration;
        }
    }, 1000);
}

window.onload = function () {
    var fiveMinutes =1,
        display = document.querySelector('#time');
    startTimer(fiveMinutes, display);
};
</script>
              
              
              
              <div class=" clock" >
                  <h2> Time Stamp:</h2>
                  <div id="time" style="font-family: 'Orbitron', sans-serif;width:10%;margin-left:45%;border: 1px solid black;border-radius: 5px 5px 5px 5px;"> </div> 
              </div>
              <?php
             // echo "This will be deleted!".$_SESSION['result'];
              echo '<br>'.'<br>'.'<h3>Currently Solved Questions'.':'.$_SESSION['attempt'].'</h3>';
              if (strpos($sign,"sqr") !== false || strpos($sign,"cr") !== false ) {
              echo "Enter Upto 3 decimal Places!!";
              }
             
             if (strpos($sign,"cr") !== false) {
     echo '<h1 >'.'&#8731;'.$_SESSION['value1'].' '.'</h1>'.'<h1 class= "mobile" style="padding-right:3%;margin-top:-1%"></h1>';
             }
              if (strpos($sign,"sqr") !== false) {
     echo '<h1 >'.'&radic;'.$_SESSION['value1'].' '.'</h1>'.'<h1 class= "mobile" style="padding-right:3%;margin-top:-1%"></h1>';
             }
              if (strpos($sign,"squares") !== false) {
     echo '<h1 >'.$_SESSION['value1'].'<sup>2</sup> '.'</h1>'.'<h1 class= "mobile" style="padding-right:3%;margin-top:-1%"></h1>';
             }
              
              if (strpos($sign,"cubes") !== false) {
     echo '<h1 >'.$_SESSION['value1'].'<sup>3</sup> '.'</h1>'.'<h1 class= "mobile" style="padding-right:3%;margin-top:-1%"></h1>';
             }
             
            ?>
              
              <div class="form-group" style="width:auto;text-align:center;">
                <input type="text" class="form-control" style="width:30%; align:center;margin-left:35%;" name="answer" >
              </div>
                <br>
              <input  class="btn btn-success" type="submit" name="submitquiz" value="Submit Answer"/>
              
            </form>          
          </div>
      </div>
        
    </div>
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    </body>
</html>
