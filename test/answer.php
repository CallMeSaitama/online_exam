<?php
 //entry.php
 include("../inc/functions.php");
 session_start();

 if(!isset($_SESSION["customer_email"]))
 {
      header("location:../index.php");

    }
 else {
     $customer_email = $_SESSION["customer_email"]; 
     $con = vmf_connect_db() or die('q0issue');
     $sql = "select id, firstname, lastname, phone, user_elo from info WHERE email='" . $customer_email . "'";
        
        $query = mysqli_query($con, $sql) or die('q1issue');
        while($rs = mysqli_fetch_assoc($query)){
            $user_id = $rs['id'];
            $user_firstname = $rs['firstname'];
            $user_lastname = $rs['lastname'];
            $user_phone = $rs['phone'];
            $user_elo = $rs['user_elo'];
         }
 
 }
    $sign = $_GET['sign'];
    $name = $_GET['name'];    
    $urlquery = 'sign=' . $sign . '&' . 'name='. $name. '&' .'count='.$num_count++;
    
    if(isset($_POST['submitquiz'])){
        $end=0;
        $end=microtime(true);
        $_SESSION['end']=$end;
        function timestamp(){
            $duration = $_SESSION['end']-$_SESSION['start'];
            $hours = (int)($duration/60/60);
            $minutes = (int)($duration/60)-$hours*60;
            $seconds = (int)$duration-$hours*60*60-$minutes*60;
            return $seconds;
        }
        
        $answer=$_POST['answer'];
        $sign = $_GET['sign'];
    
        
    
       if(empty($answer)){
           $remarks= "Not Answered!";
            $answer="N/A";
        } 
        elseif(bccomp($_SESSION['result'], $answer)==0){   $remarks= "The answer is correct!";
            $_SESSION['correct']++;
            
            $_SESSION['time']+=timestamp();
    }
        
        elseif($answer!=$_SESSION['result']){
           $remarks= "Not a correct Answer!";
            if(!empty($result)){ $_SESSION['wrong']++;
            
            $_SESSION['time']+=timestamp();
                               }
        }
       
        $_SESSION['remarks']=$remarks;
    }

    $_SESSION['average']=round($_SESSION['time']/$_SESSION['attempt'],2);
    $_SESSION['left']=$_SESSION['questions'];
    $result= $_SESSION['result'];
    if($sign=='a'){
        $sign = utf8_encode('+');
       $_SESSION['result']= $value1+$value2;
         $_SESSION['dbset']='p1';
    }
    elseif($sign=='s'){
        $sign = utf8_encode('-');
        $_SESSION['result']= $value1-$value2;
         $_SESSION['dbset']='p2';
    }
    elseif($sign=='m'){
        $sign = utf8_encode('x');
        $_SESSION['result']= $value1*$value2;
         $_SESSION['dbset']='p3';
    }
    elseif($sign=='d'){
        $sign = utf8_encode('/');
        $_SESSION['result']= $value1/$value2;
         $_SESSION['dbset']='p4';
    }
    if(!empty($result)){    
        $_SESSION['attempt']++;
       $_SESSION['questions']--; 
    }
    if(empty($result)){
        $result="N/A";$answer="N/A";
    }
    $count=0;
    $_SESSION['sign']=$sign;
$percentage=0;
    $percentage= $_SESSION['correct']/$_SESSION['totalquestions'];
    if($_SESSION['correct']==0)
        $percentage=0;
        
$dbset=$_SESSION['dbset'];

 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Online Examination System</title>
    <!-- Bootstrap core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <style>
      body {
        padding-top: 54px;
      }
      @media (min-width: 992px) {
        body {
          padding-top: 56px;
        }
      }
    </style>
  </head>
  <body>
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" style="background-color: #1e72bd !important;">
      <div class="container">
        <a class="navbar-brand" href="#">Online Test</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="../dashboard/index.php">Home           
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link active" href="../test.php">Tests
                <span class="sr-only">(current)</span>
                </a>          
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../profile.php">Profile</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../logout.php">Logout</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- Page Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <p class="lead"><?= $a3 ?></p> 
          <form method="POST" action="<?php echo $_SERVER["PHP_SELF" . '?' . $urlquery]; ?>" id="todelete">
              <input  class="btn btn-success" type="submit" name="submitquiz" value="Submit Answer"/>
              <input  class="btn btn-danger" type="submit" name="exitquiz" value="Exit Test"/>
            </form>
<?php
#Writing The score intoto database            
// This grades the quiz once they have clicked the submit button
if (isset($_POST['submitquiz'])) {
    if($_SESSION['questions']<=0){
        echo '<h3 style="padding:14px 16px;">'."You have completed the Test!!".'</h3>';
    }
    
    echo "<div class='container'>
  <h2>Result</h2>    
  
  <table class='table' border=20>
    <tbody>
      <tr>
        <td>Correct Answer</td>
        <td>".$result."</td>
      </tr>
      <tr>
        <td>Your Answer</td>
        <td>".$answer."</td>
      </tr>
      <tr>
        <td>Remarks</td>
        <td>".$remarks."</td>
      </tr>
    </tbody>
  </table>

</div>";
    
    echo "<div class='container'>
  <h2>Scoreboard</h2>    
  
  <table class='table' border=20>
    <thead>
      <tr>
        <th>Attributes</th>
        <th>Test Statistics</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Questions Left</td>
        <td>".$_SESSION['questions']."</td>
      </tr>
      <tr>
        <td>Correct</td>
        <td>".$_SESSION['correct']."</td>
      </tr>
      <tr>
        <td>Wrong</td>
        <td>".$_SESSION['wrong']."</td>
      </tr>
      <tr>
        <td>Time Taken for the Previous Sum</td>
        <td>".timestamp()." sec </td>
      </tr>
      <tr>
        <td>Average time taken to solve 1 Question</td>
        <td>".$_SESSION['average']."</td>
      </tr>
    </tbody>
  </table>

</div>";
   $con = vmf_connect_db() or die('q2 issue');
    $answer=$_POST['answer'];
        if($_SESSION['questions']>0){
            echo '<br>
        <a href="../test/index.php'. '?' . $urlquery.'" class="btn btn-primary" role="button">Continue Test</a>
       ';
        }
         echo '
        
        <script>
        console.log("i should be working and deleting");
        var element = document.getElementById("todelete");
        var element1 = document.getElementById("testname");
        element.parentNode.removeChild(element);
        element.parentNode.removeChild(element1);
        </script>';  
}
                echo ' <a href="../misctest.php" class="btn btn-danger" role="button">Quit Test</a>';
            
            
         $sqltest1 = "UPDATE practice SET $dbset = $percentage WHERE id= $user_id"; 
   		$querytest = mysqli_query($con,$sqltest1) or die('Querry Not connected');
            
            
            
?>     
          </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  </body>
</html>
