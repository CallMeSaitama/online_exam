<?php
 //entry.php
 include("inc/functions.php");
 session_start();

 if(!isset($_SESSION["customer_email"]))
 {
      header("location:index.php");
      
  
    }
 else {
     $customer_email = $_SESSION["customer_email"]; 
     $con = vmf_connect_db();
     $sql = "select id, firstname, lastname, phone, user_elo from info WHERE email='" . $customer_email . "'";   
        $query = mysqli_query($con, $sql);
        while($rs = mysqli_fetch_assoc($query)){
            $user_id = $rs['id'];
            $user_firstname = $rs['firstname'];
            $user_lastname = $rs['lastname'];
            $user_phone = $rs['phone'];
            $user_elo = $rs['user_elo'];
         }
        if(!$_SESSION['wrong'] && !$_SESSION['rigt']&& !$_SESSION['attempt']){
            $_SESSION['wrong']=0;
            $_SESSION['correct']=0;
            $_SESSION['attempt']=0;
            $_SESSION['average']=0;
        }
        $value1=mt_rand(500,1000);
        $value2=mt_rand(1,499);
        $_SESSION['value1']=$value1;
        $_SESSION['value2']=$value2;
     $sign = $_GET['sign'];
    if($sign=='a'){
        $sign = utf8_encode('+');
        $_SESSION['result']= $value1+$value2;
    }
    elseif($sign=='s'){
        $sign = utf8_encode('-');
        $_SESSION['result']= $value1-$value2;
    }
    elseif($sign=='m'){
        $sign = utf8_encode('x');
        $_SESSION['result']= $value1*$value2;
    }
    elseif($sign=='d'){
        $sign = utf8_encode('/');
        $_SESSION['result']= $value1/$value2;
    }
 }
    $sign = $_GET['sign'];
    $name = $_GET['name'];    
    $urlquery = 'sign=' . $sign . '&' . 'name='. $name. '&' .'attempt='.$_SESSION['attempt'];
    if($sign=='a'){
        $sign = utf8_encode('+');
        $result= $value1+$value2;
    }
    elseif($sign=='s'){
        $sign = utf8_encode('-');
        $result= $value1-$value2;
    }
    elseif($sign=='m'){
        $sign = utf8_encode('x');
        $result= $value1*$value2;
    }
    elseif($sign=='d'){
        $sign = utf8_encode('/');
        $result= $value1/$value2;
    }
    $count=0;
    $_SESSION['sign']=$sign;
    $_SESSION['url']=$urlquery;
 ?>

<!DOCTYPE html>
<html>
    <head>
     
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Online Examination System</title>

         <!-- Bootstrap CSS CDN -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" integrity="sha384-3AB7yXWz4OeoZcPbieVW64vVXEwADiYyAEhwilzWsLw+9FgqpyjjStpPnpBO8o8S" crossorigin="anonymous">
        <!-- Our Custom CSS -->
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="css/dashboard.css">
    </head>
    <body>



        <div class="wrapper">
            <!-- Sidebar Holder -->
            <nav id="sidebar">
                <div class="sidebar-header">
                    <p class="text-center"><img src="img/logo.png" height="50%" width="50%"></p>
                    <h3>Online Examination System</h3>
                    
                </div>

             <ul class="list-unstyled components">
                    <p>Learning Platform</p>
                    <li class="active">
                       <a href="dashboard/index.php" data-toggle="collapse" aria-expanded="false">Home</a>
                        <ul class="collapse list-unstyled" id="homeSubmenu">
                            <li><a href="test.php">Tests</a></li>
                            <li><a href="misctest.php">Practice Engine</a></li>
                            <li><a href="profile.php">Profile</a></li>
                            <li><a href="logout.php">Logout</a></li>
                        </ul>
                    </li>
                    <li>
                    
                        <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false">Learning Modules</a>
                        <ul class="collapse list-unstyled" id="pageSubmenu">
                            <li><a href="page-chat.php">Learning Modules</a></li>
                            <li><a href="page-chat.php" data-toggle="modal" data-target="#practice">Practice Engine</a></li>
                            
                        </ul>
                        <a href="#">About</a>
                    </li>
                    
                </ul>

                <ul class="list-unstyled CTAs">
                    &copy; <?php echo date("Y"); ?> 
                </ul>
            </nav>

            <!-- Page Content Holder -->
            <div id="content">

                <nav class="navbar navbar-default text-center">
                    <div class="container-fluid">

                        <div class="navbar-header">
                            <button type="button" id="sidebarCollapse" class="navbar-btn">
                                <span></span>
                                <span></span>
                                <span></span>
                            </button>
                        </div>
                        
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right" id="Navigation">
                                <li><a href="dashboard/index.php" style="color: #23527c;">Dashboard</a></li>
                                <li><a href="test.php" style="color: #23527c;">Tests</a></li>
                                <li><a href="misctest.php">Practice Engine</a></li>
                                <li><a href="profile.php" >Profile</a></li>
                                <li><a href="logout.php">Logout</a></li>
                                
                            </ul>
                        </div>
                    </div>
                </nav>

                <h2> </h2>
              
          
                
                <h3><?php echo $user_firstname; ?>'s Practice Engine</h3>
                <br>
                <div class="line"></div>
<!--                row 1 begins-->
                <div class="row text-left" style="color:black !important;padding-left:5%;padding-right:5%;">
                            <h2>Customize Your Test Below!</h2><br>

                    <div style="display:none" id="thankyou_message">
                    <h2>Please Wait..</h2>
                    </div>
                    <form method="POST" action="<?php echo "test/fscsc.php".'?'.$_SESSION['url']; ?>" id="todelete">

                    <div class="messages"></div>

                    <div class="controls">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_name"><ol><li>Choose the number of Digits you want to practice:</li></ol> </label>
                                    <div class="radio" style="padding-left:5%;">
                                        <label><input type="radio" name="digits" value="2" required>2 Digits</label>
                                        <label><input type="radio" name="digits" value="3">3 Digits</label>
                                        <label><input type="radio" name="digits" value="4">4 Digits</label>
                                        
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_name"><ol><li>Choose the number of Questions you want to practice:</li></ol></label>
                                    <div class="radio" style="padding-left:5%;" >
                                        <label ><input type="radio" name="questions" value="10" required>10</label>&nbsp;
                                        <label><input type="radio" name="questions" value="20">20</label>
                                        <label><input type="radio" name="questions" value="30">30</label>
                                        <label><input type="radio" name="questions" value="40">40 or Above</label>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                           
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="padding-left:10%;">
                                <input type="submit" class="btn btn-success btn-send" name="submitquiz" value="Start Test!">
                            </div>
                        </div>
                        
                    </div>

                    </form>
                          </div>


                    <!-- Submit the Form to Google Using "AJAX" -->
                    <script data-cfasync="false" type="text/javascript"
                    src="https://cdn.rawgit.com/dwyl/learn-to-send-email-via-google-script-html-no-server/master/form-submission-handler.js"></script>
                    <!-- <script data-cfasync="false" type="text/javascript"
                    src="/form-submission-handler.js"></script> -->
                  <!-- END -->
             
                <div class="line"></div>              
<!--        row 17 ends         -->
                <h3></h3>
                <p style="color:white;">Learning High Speed Vedic Mathematics will enable you to calculate much faster compared to the conventional system. You will be able to do seemingly difficult calculations like 998 x 997 in split seconds which will make a remarkable difference to your confidence and self esteem. Our courses are structured based on 15+ years of experience in teaching mathematics for students and teachers of all levels.</p>
            </div>      
          </div>
        <!-- jQuery CDN -->
         <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
         <!-- Bootstrap Js CDN -->
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
         <script type="text/javascript">
             $(document).ready(function () {
                 $('#sidebarCollapse').on('click', function () {
                     $('#sidebar').toggleClass('active');
                     $(this).toggleClass('active');
                 });
             });
         </script>
        <script type="text/javascript">
         var height = window.innerHeight ;  
         var sub = 0.2*height;    
            document.getElementById("iframe1").height = height-sub;
        </script>
      
    </body>
</html>