    <?php
     //entry.php
     include("inc/functions.php");
     session_start();

     if(!isset($_SESSION["customer_email"]))
     {
          header("location:index.php");


        }
     else {
         $space = " ";
         $per = "%";
        $customer_email = $_SESSION["customer_email"];
       $con = vmf_connect_db();
       $sql = "select id, firstname, lastname, phone from info WHERE email='" . $customer_email . "'";

        $query = mysqli_query($con, $sql);
        while($rs = mysqli_fetch_assoc($query)){
            $user_id = $rs['id'];
            $user_firstname = $rs['firstname'];
            $user_lastname = $rs['lastname'];
            $user_phone = $rs['phone'];


         }
         $sqltest = "select * from test WHERE id='" . $user_id . "'";
         $querytest = mysqli_query($con, $sqltest);
        while($rstest = mysqli_fetch_assoc($querytest)){

            $user_t1 = $rstest['t1'];
            $user_t2 = $rstest['t2'];
            $user_t3 = $rstest['t3'];
            $user_t4 = $rstest['t4'];
         }
     }
     ?>

    <!DOCTYPE html>
    <html>
        <head>

            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">

            <title>Online Examination System</title>

             <!-- Bootstrap CSS CDN -->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" integrity="sha384-3AB7yXWz4OeoZcPbieVW64vVXEwADiYyAEhwilzWsLw+9FgqpyjjStpPnpBO8o8S" crossorigin="anonymous">
            <!-- Our Custom CSS -->
            <link rel="stylesheet" href="style.css">
            <link rel="stylesheet" href="css/dashboard.css">
        </head>
        <body>



            <div class="wrapper"  style="width:100%!important;">
                <!-- Sidebar Holder -->
                <nav id="sidebar">
                    <div class="sidebar-header">
    <!--                    <p class="text-center"><img src="img/logo.png" height="50%" width="50%"></p>-->
                        <h3>Test Section</h3>

                    </div>

                 <ul class="list-unstyled components">
                        <p>Learning Platform</p>
                        <li class="active">
                           <a href="dashboard/index.php" data-toggle="collapse" aria-expanded="false">Home</a>
                            <ul class="collapse list-unstyled" id="homeSubmenu">
                                <li><a href="test.php">Tests</a></li>
                                <li><a href="misctest.php">Practice Engine</a></li>
                                <li><a href="profile.php">Profile</a></li>
                                <li><a href="logout.php">Logout</a></li>
                            </ul>
                        </li>
                        <li>

                            <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false">Learning Modules</a>
                            <ul class="collapse list-unstyled" id="pageSubmenu">
                                <li><a href="page-chat.php">Learning Modules</a></li>
                                <li><a href="page-chat.php" data-toggle="modal" data-target="#practice">Practice Engine</a></li>

                            </ul>
                            <a href="#">About</a>
                        </li>

                    </ul>
<!--done-->
    <!--                <ul class="list-unstyled CTAs">-->
    <!--                    &copy; --><?php //echo date("Y"); ?><!-- Online Test-->
    <!--                </ul>-->
                </nav>

                <!-- Page Content Holder -->
                <div id="content">

                    <nav class="navbar navbar-default text-center">
                        <div class="container-fluid">

                            <div class="navbar-header">
                                <button type="button" id="sidebarCollapse" class="navbar-btn">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </button>
                            </div>

                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1"  style="padding-left:10%;width:1000px;">
                                <ul class="nav navbar-nav navbar-right" id="Navigation">
                                    <li><a href="dashboard/index.php" style="color: #23527c;">Dashboard</a></li>
                                    <li><a href="#" style="color: #23527c;">Tests</a></li>
                                    <li><a href="profile.php" >Profile</a></li>
                                    <li><a href="logout.php">Logout</a></li>

                                </ul>
                            </div>
                        </div>
                    </nav>

                    <div class="line"></div>

                    <h3><?php echo $user_firstname; ?>'s Tests</h3>
                    <div class="line"></div>
                    <br>
                    <h4 style="text-align:center"></h4>
                   <br>
                    <div class="row" style="padding-left:10%;width:1000px;">
                        <div class="col-sm-3">
                        <div class="cards" id="add" style="text-align:center;margin-left:10%; padding:14px 16px;">
                         <i class="fas fa-plus fa-4x"></i>
                         <h4>Computer</h4>
                            <a href="quiz/index.php?test=1&name=Computer">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add" style="margin-top: 10%">
                              Launch Test
                            </button>
                                </a>
                             <p style="color: black; padding-top:5%;">
                                 Previous Score : <?= $space . $user_t1 . $per?>
                            </p>
                        </div>
                        </div>
                        <div class="col-sm-3">
                        <div class="cards" id="add" style="text-align:center; margin-left:10%; padding:14px 16px;">
                         <i class="fas fa-plus fa-4x"></i>
                        <h4>Environmental Studies</h4>
                    <!-- Button trigger modal -->

                            <p></p>
                            <a href="quiz/index.php?test=2&name=EVS">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add" style="margin-top: 10%">
                              Launch Test
                            </button>
                                </a>
                             <p style="color: black; padding-top:5%;">
                                 Previous Score : <?= $space . $user_t2 . $per?>
                            </p>

                        </div>
                        </div>
                        <div class="col-sm-3">
                         <div class="cards" id="add" style="text-align:center; margin-left:10%;padding:14px 16px;">
                         <i class="fas fa-plus fa-4x"></i>
                         <h4>History</h4>
                            <a href="quiz/index.php?test=3&name=History">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add" style="margin-top: 10%">
                              Launch Test
                            </button>
                                </a>
                             <p style="color: black; padding-top:5%;">
                                 Previous Score : <?= $space . $user_t3 . $per?>
                            </p>
                        </div>
                        </div>
                        <div class="col-sm-3">
                        <div class="cards" id="add" style="text-align:center; margin-left:10%;padding:14px 16px;">
                         <i class="fas fa-plus fa-4x"></i>
                        <h4>Science</h4>
                            <a href="quiz/index.php?test=4&name=Science">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add" style="margin-top: 10%">
                              Launch Test
                            </button>
                                </a>
                             <p style="color: black; padding-top:5%;">
                                 Previous Score : <?= $space . $user_t4 . $per?>
                            </p>
                        </div>
                        </div>
                     </div>
                </div>
            </div>
            

            <!-- jQuery CDN -->
             <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
             <!-- Bootstrap Js CDN -->
             <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
             <script type="text/javascript">
                 $(document).ready(function () {
                     $('#sidebarCollapse').on('click', function () {
                         $('#sidebar').toggleClass('active');
                         $(this).toggleClass('active');
                     });
                 });
             </script>
            <script type="text/javascript">
             var height = window.innerHeight ;
             var sub = 0.2*height;
                document.getElementById("iframe1").height = height-sub;



            </script>

        </body>
    </html>