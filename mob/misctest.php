<?php
 $_SESSION['wrong']=0;$_SESSION['rigt']=0;$_SESSION['attempt']=0;$_SESSION['correct']=0;$_SESSION['attempt']=0;$_SESSION['average']=0;$_SESSION['time']=0;
    $_SESSION['average']=0;
    $_SESSION['dbset']=0;
 ?>

<!DOCTYPE html>
<html>
    <head>
     
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Vedic Maths Chatbot</title>

         <!-- Bootstrap CSS CDN -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" integrity="sha384-3AB7yXWz4OeoZcPbieVW64vVXEwADiYyAEhwilzWsLw+9FgqpyjjStpPnpBO8o8S" crossorigin="anonymous">
        <!-- Our Custom CSS -->
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="css/dashboard.css">
    </head>
    <body>



        <div class="wrapper">
            <!-- Sidebar Holder -->
            

            <!-- Page Content Holder -->
            <div id="content">

                <h2> </h2>
              
                <div class="line"></div>
                
                <h3></h3>
                <br>
                <div class="line"></div>
<!--                row 1 begins-->
                <div class="row text-center" style="padding-left:10%;">

                    <div class="col-md-2 col-sm-3 cards" id="add">
                     <i class="fas fa-plus fa-4x"></i>
                    <h4>Addition</h4>
                <!-- Button trigger modal -->
                        
                        <p></p>
                        <a href="misctestlimit.php?sign=a&name=Addition">
                        <button type="button" class="btn btn-primary" data-toggle="modal"  style="margin-top: 10%">
                          Practice Now!
                        </button>
                            </a>
                        <p style="color: black; padding-top:5%;">
                            
                        </p>
        
                    
                    </div>
                   <div class="col-md-1" ></div>
                    <div class="col-md-2 col-sm-3 cards">
                     <i class="fas fa-minus fa-4x"></i>
                    <h4>Subtraction</h4>
                <!-- Button trigger modal -->
                        
                        <p></p>
                        <a href="misctestlimit.php?sign=s&name=Subtraction">
                        <button type="button" class="btn btn-primary" data-toggle="modal"  style="margin-top: 10%" >
                          Practice Now!
                        </button>
                            </a>
                        <p style="color: black; padding-top:5%;">
                             
                        </p>
                          
                    </div>
                    <div class="col-md-1" ></div>
                    <div class="col-md-2 col-sm-3 cards" id="mul">
                   <i class="fas fa-times fa-4x"></i>
                    <h4>Multiplication</h4>
                <!-- Button trigger modal -->          
                        <p></p>
                        <a href="misctestlimit.php?sign=m&name=Multiplication">
                        <button type="button" class="btn btn-primary" data-toggle="modal" style="margin-top: 10%" >
                          Practice Now!
                        </button>
                            </a>
                        <p style="color: black; padding-top:5%;">
                            
                        </p>
                                    
                    </div>
                    <div class="col-md-1" ></div>
                    <div class="col-md-2 col-sm-3 cards">
                     <img src="img/div.svg" width="30%" height="10%">
                    <h4>Divison</h4>
                <!-- Button trigger modal -->    
                        <p></p>
                        <a href="misctestlimit.php?sign=d&name=Divison">
                        <button type="button" class="btn btn-primary" data-toggle="modal" style="margin-top: 10%" >
                          Practice Now!
                        </button>
                            </a>
                        <p style="color: black; padding-top:5%;">
                            
                        </p>
                         
                    </div>              
                    <div class="col-md-1" ></div>
                </div>
                <div class="line"></div>   
                <div class="row text-center" style="padding-left:10%;">

                    <div class="col-md-2 col-sm-3 cards" id="add">
                     <img src="img/frac.png" width="20%" height="10%">
                    <h4>Fractions</h4>
                <!-- Button trigger modal -->
                        
                        <p></p>
                        <a href="#">
                        <button type="button" class="btn btn-primary" data-toggle="modal" style="margin-top: 10%">
                          Practice Now!
                        </button>
                            </a>
                        <p style="color: black; padding-top:5%;">
                            
                        </p>
                        
                    
                    </div>
                   <div class="col-md-1" ></div>
                    <div class="col-md-2 col-sm-3 cards">
                     <i class="fas fa-superscript fa-4x"></i>
                    <h4>Squares</h4>
                <!-- Button trigger modal -->
                        
                        <p></p>
                        <a href="misclimit.php?sign=squares&name=Squares">
                        <button type="button" class="btn btn-primary" data-toggle="modal"  style="margin-top: 10%" >
                          Practice Now!
                        </button>
                            </a>
                        <p style="color: black; padding-top:5%;">
                            
                        </p>
                         
                    </div>
                    <div class="col-md-1" ></div>
                    <div class="col-md-2 col-sm-3 cards" id="mul">
                   <h1><strong>x<sup>3</sup></strong></h1>
                    <h4>Cubes</h4>
                <!-- Button trigger modal -->          
                        <p></p>
                        <a href="misclimit.php?sign=cubes&name=Cubes">
                        <button type="button" class="btn btn-primary" data-toggle="modal"  style="margin-top: 10%" >
                          Practice Now!
                        </button>
                            </a>
                        <p style="color: black; padding-top:5%;">
                            
                        </p>
                                   
                    </div>
                    <div class="col-md-1" ></div>
                    <div class="col-md-2 col-sm-3 cards">
                     <img src="img/srt.svg" width="40%" height="10%">
                    <h4>Square Roots</h4>
                <!-- Button trigger modal -->    
                        <p></p>
                        <a href="misclimit.php?sign=sqr&name=Sqaure Roots">
                        <button type="button" class="btn btn-primary" data-toggle="modal" style="margin-top: 10%" >
                          Practice Now!
                        </button>
                            </a>
                        <p style="color: black; padding-top:5%;">
                             
                        </p>
                         
                    </div>              
                    <div class="col-md-1" ></div>
                </div>
                
                 <div class="line"></div>
                
                
                 <div class="row text-center" style="padding-left:10%;">
                    <div class="col-md-2 col-sm-3 cards">
                     <img src="img/crt.png" width="30%" height="10%">
                    <h4>Cube Roots</h4>
                <!-- Button trigger modal -->    
                        <p></p>
                        <a href="misclimit.php?sign=cr&name=Cube Roots">
                        <button type="button" class="btn btn-primary" data-toggle="modal" style="margin-top: 10%" >
                          Practice Now!
                        </button>
                            </a>
                        <p style="color: black; padding-top:5%;">
                             
                        </p>
                         
                    </div>              
                    <div class="col-md-1" ></div>
                </div>
                
                 <div class="line"></div>
            </div>
        </div>
      
    </body>
</html>