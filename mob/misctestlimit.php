<?php
 
        $value1=mt_rand(500,1000);
        $value2=mt_rand(1,499);
        $_SESSION['value1']=$value1;
        $_SESSION['value2']=$value2;
     $sign = $_GET['sign'];
    if($sign=='a'){
        $sign = utf8_encode('+');
        $_SESSION['result']= $value1+$value2;
    }
    elseif($sign=='s'){
        $sign = utf8_encode('-');
        $_SESSION['result']= $value1-$value2;
    }
    elseif($sign=='m'){
        $sign = utf8_encode('x');
        $_SESSION['result']= $value1*$value2;
    }
    elseif($sign=='d'){
        $sign = utf8_encode('/');
        $_SESSION['result']= $value1/$value2;
    }
    $sign = $_GET['sign'];
    $name = $_GET['name'];    
    $urlquery = 'sign=' . $sign . '&' . 'name='. $name;
    if($sign=='a'){
        $sign = utf8_encode('+');
        $result= $value1+$value2;
    }
    elseif($sign=='s'){
        $sign = utf8_encode('-');
        $result= $value1-$value2;
    }
    elseif($sign=='m'){
        $sign = utf8_encode('x');
        $result= $value1*$value2;
    }
    elseif($sign=='d'){
        $sign = utf8_encode('/');
        $result= $value1/$value2;
    }
    $count=0;
    $_SESSION['sign']=$sign;
    $_SESSION['url']=$urlquery;
 ?>

<!DOCTYPE html>
<html>
    <head>
     
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Vedic Maths Chatbot</title>

         <!-- Bootstrap CSS CDN -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" integrity="sha384-3AB7yXWz4OeoZcPbieVW64vVXEwADiYyAEhwilzWsLw+9FgqpyjjStpPnpBO8o8S" crossorigin="anonymous">
        <!-- Our Custom CSS -->
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="css/dashboard.css">
    </head>
    <body>



        <div class="wrapper">
            <!-- Sidebar Holder -->
           
                <h2> </h2>
              
          
                
                <h3></h3>
                <br>
                <div class="line"></div>
<!--                row 1 begins-->
                <div class="row text-left" style="color:black !important;padding-left:5%;padding-right:5%;">
                            <h2>Customize Your Test Below!</h2><br>

                    <div style="display:none" id="thankyou_message">
                    <h2>Please Wait..</h2>
                    </div>
                    <form method="POST" action="<?php echo "test/index.php".'?'.$_SESSION['url']; ?>" id="todelete">

                    <div class="messages"></div>

                    <div class="controls">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_name"><ol><li>Choose the number of Digits you want to practice:</li></ol> </label>
                                    <div class="radio" style="padding-left:5%;">
                                        <label><input type="radio" name="digits" value="2" required>2 Digits</label>
                                        <label><input type="radio" name="digits" value="3">3 Digits</label>
                                        <label><input type="radio" name="digits" value="4">4 Digits</label>
                                        <label><input type="radio" name="digits" value="5">Above 4 Digits</label>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_name"><ol><li>Choose the number of Questions you want to practice:</li></ol></label>
                                    <div class="radio" style="padding-left:5%;" >
                                        <label ><input type="radio" name="questions" value="10" required>10</label>&nbsp;
                                        <label><input type="radio" name="questions" value="20">20</label>
                                        <label><input type="radio" name="questions" value="30">30</label>
                                        <label><input type="radio" name="questions" value="40">40 or Above</label>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                           
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="padding-left:10%;">
                                <input type="submit" class="btn btn-success btn-send" name="submitquiz" value="Start Test!">
                            </div>
                        </div>
                        
                    </div>

                    </form>
                          </div>


                    <!-- Submit the Form to Google Using "AJAX" -->
                    <script data-cfasync="false" type="text/javascript"
                    src="https://cdn.rawgit.com/dwyl/learn-to-send-email-via-google-script-html-no-server/master/form-submission-handler.js"></script>
                    <!-- <script data-cfasync="false" type="text/javascript"
                    src="/form-submission-handler.js"></script> -->
                  <!-- END -->
             
                <div class="line"></div>              
<!--        row 17 ends         -->
                
            </div>
      
    </body>
</html>