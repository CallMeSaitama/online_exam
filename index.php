<?php
  require_once('inc/functions.php');

  if ( isset( $_POST['formSubmit'] ) ) {
      $customer_email = null;
      $customer_pass = null;
      $user_does_not_exist = false;
      $error = false;

      $customer_email = isset( $_POST['customer_email'] ) ? $_POST['customer_email'] : null;
      $customer_pass = isset( $_POST['customer_pass'] ) ? $_POST['customer_pass'] : null;
      $customer_pass = md5($customer_pass);

      if( $customer_email AND $customer_pass ) {
        if ( $user_does_not_exist = check_user_exists( $customer_email ) AND $pass_check = check_pass( $customer_email, $customer_pass) ) {
            session_start();
            $_SESSION['customer_email'] = $customer_email;
        }
    } else {
        $error = true;
    }
}

if(isset($_SESSION["customer_email"]))
{
     header("location:dashboard/index.php");
}

?>

<!doctype html>
<html lang="en">
  <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-116564296-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-116564296-1');
        </script>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="noindex, nofollow">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <style>
        .a1{
            color: black;
        }
        .a1:hover {
            color : #4c4c4c;
        }
      
      </style>  

    <title>Online Examination System</title>
  </head>

  <body class="text-center">
    <div class="container">

        <div class="form-signin">
        <!--    <?php
            if ( isset( $_POST['formSubmit'] ) ) {
              if ( !$user_does_not_exist ) { ?>
                <div class="alert alert-danger" role="alert">
                  User does not exist
                </div>
            <?php } else { ?>
                <div class="alert alert-success" role="alert">
                  Login Successful. Redirecting...
                </div>
           <?php }

              if ( $error ) { ?>
                  <div class="alert alert-danger" role="alert">
                    Please enter the details correctly
                  </div>
              <?php }
          }
          ?>-->
        </div>
      <form class="form-signin" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">

        <img class="mx-auto d-block mb-3" src="" alt="">


        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" id="inputEmail" class="form-control" name="customer_email" placeholder="Email address" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" name="customer_pass" class="form-control" placeholder="Password" required>

        <button class="btn btn-lg btn-warning btn-block" name="formSubmit" type="submit">Sign in</button>
          <p>Don't have an account? Buy premium access to our system. <a class="a1" href="http://localhost/online_exam/register.php">here</a></p>
<!--        <p class="mt-2 mb-2 text-white font-weight-light">&copy; <?php //echo date('Y'); ?>, VedicMathsIndia.Org</p>-->
      </form>


    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>
