<?php
 //entry.php
 include("inc/functions.php");
 session_start();

 if(!isset($_SESSION["customer_email"]))
 {
      header("location:index.php");
      
  
    }
 else {
     $space = " ";
     $per = "%";
    $customer_email = $_SESSION["customer_email"]; 
   $con = vmf_connect_db();
   $sql = "select id, firstname, lastname, phone from info WHERE email='" . $customer_email . "'";
        
    $query = mysqli_query($con, $sql);
    while($rs = mysqli_fetch_assoc($query)){
        $user_id = $rs['id'];
        $user_firstname = $rs['firstname'];
        $user_lastname = $rs['lastname'];
        $user_phone = $rs['phone'];
        
        
     }
     $sqltest = "select * from test WHERE id='" . $user_id . "'";
     $querytest = mysqli_query($con, $sqltest);
    while($rstest = mysqli_fetch_assoc($querytest)){
        
        $user_t1 = $rstest['t1'];
        $user_t2 = $rstest['t2'];
        $user_t3 = $rstest['t3'];
        $user_t4 = $rstest['t4'];

     }
 }
    unset($_SESSION['wrong'],$_SESSION['rigt'],$_SESSION['attempt'],$_SESSION['correct'],$_SESSION['attempt'],$_SESSION['average'],$_SESSION['time']);
    $_SESSION['average']=0;
 ?>

<!DOCTYPE html>
<html>
    <head>
     
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Vedic Maths Chatbot</title>

         <!-- Bootstrap CSS CDN -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" integrity="sha384-3AB7yXWz4OeoZcPbieVW64vVXEwADiYyAEhwilzWsLw+9FgqpyjjStpPnpBO8o8S" crossorigin="anonymous">
        <!-- Our Custom CSS -->
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="css/dashboard.css">
    </head>
    <body>



        <div class="wrapper">
            <!-- Sidebar Holder -->
            <nav id="sidebar">
                <div class="sidebar-header">
                    <p class="text-center"><img src="img/logo.png" height="50%" width="50%"></p>
                    <h3>Vedic Maths Chatbot</h3>
                    
                </div>

             <ul class="list-unstyled components">
                    <p>Learning Platform</p>
                    <li class="active">
                       <a href="dashboard/index.php" data-toggle="collapse" aria-expanded="false">Home</a>
                        <ul class="collapse list-unstyled" id="homeSubmenu">
                            <li><a href="test.php">Tests</a></li>
                            <li><a href="misctest.php">Practice Engine</a></li>
                            <li><a href="profile.php">Profile</a></li>
                            <li><a href="logout.php">Logout</a></li>
                        </ul>
                    </li>
                    <li>
                    
                        <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false">Learning Modules</a>
                        <ul class="collapse list-unstyled" id="pageSubmenu">
                            <li><a href="page-chat.php">Learning Modules</a></li>
                            <li><a href="page-chat.php" data-toggle="modal" data-target="#practice">Practice Engine</a></li>
                            
                        </ul>
                        <a href="https://vedicmathsindia.org/chatbot-V1/">About</a>
                    </li>
                    
                </ul>

                <ul class="list-unstyled CTAs">
                    &copy; <?php echo date("Y"); ?> Vedic Maths India
                </ul>
            </nav>

            <!-- Page Content Holder -->
            <div id="content">

                <nav class="navbar navbar-default text-center">
                    <div class="container-fluid">

                        <div class="navbar-header">
                            <button type="button" id="sidebarCollapse" class="navbar-btn">
                                <span></span>
                                <span></span>
                                <span></span>
                            </button>
                        </div>
                        
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right" id="Navigation">
                                <li><a href="dashboard/index.php" style="color: #23527c;">Dashboard</a></li>
                                <li><a href="test.php" style="color: #23527c;">Tests</a></li>
                                <li><a href="misctest.php">Practice Engine</a></li>
                                <li><a href="profile.php" >Profile</a></li>
                                <li><a href="logout.php">Logout</a></li>
                                
                            </ul>
                        </div>
                    </div>
                </nav>

                <h2> </h2>
              
                <div class="line"></div>
                
                <h3><?php echo $user_firstname; ?>'s Practice Engine</h3>
                <br>
                <div class="line"></div>
<!--                row 1 begins-->
                <div class="row text-center" style="padding-left:10%;">

                    <div class="col-md-2 col-sm-3 cards" id="add">
                     <i class="fas fa-plus fa-4x"></i>
                    <h4>Addition</h4>
                <!-- Button trigger modal -->
                        
                        <p></p>
                        <a href="misctestlimit.php?sign=a&name=Addition">
                        <button type="button" class="btn btn-primary" data-toggle="modal"  style="margin-top: 10%">
                          Practice Now!
                        </button>
                            </a>
        
                    
                    </div>
                   <div class="col-md-1" ></div>
                    <div class="col-md-2 col-sm-3 cards">
                     <i class="fas fa-minus fa-4x"></i>
                    <h4>Subtraction</h4>
                <!-- Button trigger modal -->
                        
                        <p></p>
                        <a href="misctestlimit.php?sign=s&name=Subtraction">
                        <button type="button" class="btn btn-primary" data-toggle="modal"  style="margin-top: 10%" >
                          Practice Now!
                        </button>
                            </a>
                          
                    </div>
                    <div class="col-md-1" ></div>
                    <div class="col-md-2 col-sm-3 cards" id="mul">
                   <i class="fas fa-times fa-4x"></i>
                    <h4>Multiplication</h4>
                <!-- Button trigger modal -->          
                        <p></p>
                        <a href="misctestlimit.php?sign=m&name=Multiplication">
                        <button type="button" class="btn btn-primary" data-toggle="modal" style="margin-top: 10%" >
                          Practice Now!
                        </button>
                            </a>
                                    
                    </div>
                    <div class="col-md-1" ></div>
                    <div class="col-md-2 col-sm-3 cards">
                     <img src="img/div.svg" width="40%" height="40%">
                    <h4>Divison</h4>
                <!-- Button trigger modal -->    
                        <p></p>
                        <a href="misctestlimit.php?sign=d&name=Divison">
                        <button type="button" class="btn btn-primary" data-toggle="modal" style="margin-top: 10%" >
                          Practice Now!
                        </button>
                            </a>
                         
                    </div>              
                    <div class="col-md-1" ></div>
                </div>
                <div class="line"></div>   
                <div class="row text-center" style="padding-left:10%;">

                    <div class="col-md-2 col-sm-3 cards" id="add">
                     <img src="img/frac.png" width="40%" height="30%">
                    <h4>Fractions</h4>
                <!-- Button trigger modal -->
                        
                        <p></p>
                        <a href="#">
                        <button type="button" class="btn btn-primary" data-toggle="modal" style="margin-top: 10%">
                          Practice Now!
                        </button>
                            </a>
                        
                    
                    </div>
                   <div class="col-md-1" ></div>
                    <div class="col-md-2 col-sm-3 cards">
                     <i class="fas fa-superscript fa-4x"></i>
                    <h4>Squares</h4>
                <!-- Button trigger modal -->
                        
                        <p></p>
                        <a href="misclimit.php?sign=squares&name=Squares">
                        <button type="button" class="btn btn-primary" data-toggle="modal"  style="margin-top: 10%" >
                          Practice Now!
                        </button>
                            </a>
                         
                    </div>
                    <div class="col-md-1" ></div>
                    <div class="col-md-2 col-sm-3 cards" id="mul">
                   <h1><strong>x<sup>3</sup></strong></h1>
                    <h4>Cubes</h4>
                <!-- Button trigger modal -->          
                        <p></p>
                        <a href="misclimit.php?sign=cubes&name=Cubes">
                        <button type="button" class="btn btn-primary" data-toggle="modal"  style="margin-top: 10%" >
                          Practice Now!
                        </button>
                            </a>
                                   
                    </div>
                    <div class="col-md-1" ></div>
                    <div class="col-md-2 col-sm-3 cards">
                     <img src="img/srt.svg" width="40%" height="20%">
                    <h4>Square Roots</h4>
                <!-- Button trigger modal -->    
                        <p></p>
                        <a href="misclimit.php?sign=sqr&name=Sqaure Roots">
                        <button type="button" class="btn btn-primary" data-toggle="modal" style="margin-top: 10%" >
                          Practice Now!
                        </button>
                            </a>
                         
                    </div>              
                    <div class="col-md-1" ></div>
                </div>
                
                 <div class="line"></div>
                
                
                 <div class="row text-center" style="padding-left:10%;">
                    <div class="col-md-2 col-sm-3 cards">
                     <img src="img/crt.png" width="50%" height="50%">
                    <h4>Cube Roots</h4>
                <!-- Button trigger modal -->    
                        <p></p>
                        <a href="misclimit.php?sign=cr&name=Cube Roots">
                        <button type="button" class="btn btn-primary" data-toggle="modal" style="margin-top: 10%" >
                          Practice Now!
                        </button>
                            </a>
                         
                    </div>              
                    <div class="col-md-1" ></div>
                </div>
                
                 <div class="line"></div>
                
                
                
<!--        row 17 ends         -->
                <h3>About Vedic Maths</h3>
                <p>Learning High Speed Vedic Mathematics will enable you to calculate much faster compared to the conventional system. You will be able to do seemingly difficult calculations like 998 x 997 in split seconds which will make a remarkable difference to your confidence and self esteem. Our courses are structured based on 15+ years of experience in teaching mathematics for students and teachers of all levels.</p>
            </div>      
          </div>
        <!-- jQuery CDN -->
         <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
         <!-- Bootstrap Js CDN -->
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
         <script type="text/javascript">
             $(document).ready(function () {
                 $('#sidebarCollapse').on('click', function () {
                     $('#sidebar').toggleClass('active');
                     $(this).toggleClass('active');
                 });
             });
         </script>
        <script type="text/javascript">
         var height = window.innerHeight ;  
         var sub = 0.2*height;    
            document.getElementById("iframe1").height = height-sub;
        </script>
      
    </body>
</html>